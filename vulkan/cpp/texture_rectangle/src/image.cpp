#include "device.h"
#include "image.h"

/**
 * Creates an image
 *
 * @param width The width of the image in pixels.
 * @param height The height of the image in pixels.
 * @param format The image format of the color (e.g. VK_FORMAT_R8G8B8A8_UNORM).
 * @param tiling Whether to use linear or optimal tiling.
 * @param usage Bitmask indicating how the image will be accessed.
 * @param properties Bitmask indicating kind of memory (e.g. VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT).
 * @param image The image object.
 * @param imageMemory The memory to be bound to the image object.
 */
bool image_create(Image *image, Device *device, uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling,
		      VkImageUsageFlags usage, VkMemoryPropertyFlags properties) {
    // Create a Vulkan image object for accessing pixel (or 'texel') values in the buffer
    VkImageCreateInfo imageInfo = {};
    imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageInfo.imageType = VK_IMAGE_TYPE_2D;
    imageInfo.extent.width = width;
    imageInfo.extent.height = height;
    imageInfo.extent.depth = 1;
    imageInfo.mipLevels = 1;
    imageInfo.arrayLayers = 1;
    imageInfo.format = format;
    imageInfo.tiling = tiling;
    imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageInfo.usage = usage;
    imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageInfo.flags = 0;

    // n.b. VK_IMAGE_TYPE_1D can be used to store a gradient.

    image->vk_device = device->device;
    if (vkCreateImage(image->vk_device, &imageInfo, nullptr, &(image->vk_image)) != VK_SUCCESS) {
	fprintf(stderr, "Failed to create image!");
	    return false;
    }

    // Determine how much memory to allocate
    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(image->vk_device, image->vk_image, &memRequirements);

    // Allocate the appropriate amount of memory
    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = device_find_memory_type(device, memRequirements.memoryTypeBits, properties);

    if (vkAllocateMemory(image->vk_device, &allocInfo, nullptr, &(image->vk_memory)) != VK_SUCCESS) {
	fprintf(stderr, "Failed to allocate image memory!");
	    return false;
    }

    // Bind the texture image and memory buffer
    vkBindImageMemory(image->vk_device, image->vk_image, image->vk_memory, 0);
	return true;
}

bool image_destroy(Image *image) {
    vkDestroyImage(image->vk_device, image->vk_image, nullptr);
    vkFreeMemory(image->vk_device, image->vk_memory, nullptr);
}
