/* Vulkan Device */
#ifndef DEVICE_H
#define DEVICE_H

#include "utils.h"

#include <assert.h>
#include <string.h>
#include <stdio.h>

#include <vulkan/vulkan.h>

const uint32_t num_validation_layers = 1;
extern const char *validation_layers[num_validation_layers];

const uint32_t num_device_extensions = 1;
extern const char *device_extensions[num_device_extensions];

#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

struct SwapChainSupportDetails {
    VkSurfaceCapabilitiesKHR capabilities;
    VkSurfaceFormatKHR formats[MAX_ARRAY];
    uint32_t num_formats;

    VkPresentModeKHR present_modes[MAX_ARRAY];
    uint32_t num_present_modes;
};

struct QueueFamilyIndices {
    int graphicsFamily = -1;
    int presentFamily = -1;

    bool isComplete() {
    return graphicsFamily >= 0 && presentFamily >= 0;
    }
};

struct Device {
    VkInstance instance;
    VkSurfaceKHR surface;
    VkDevice device;
    VkQueue graphics_queue;
    VkQueue present_queue;

    // Private
    VkPhysicalDevice physical_device;
    VkDebugReportCallbackEXT _debug_callback;
};

bool device_setup_debug_callback(Device *device, PFN_vkDebugReportCallbackEXT callback);

bool device_create_instance(Device *device, const char *extensions[], int32_t num_extensions);

bool device_pick_physical_device(Device *device);

bool device_create_logical_device(Device *device);

QueueFamilyIndices device_find_queue_families(Device *device, VkPhysicalDevice physical_device);

uint32_t device_find_memory_type(Device *device, uint32_t typeFilter, VkMemoryPropertyFlags properties);

SwapChainSupportDetails device_get_swap_chain_support(Device *device);

QueueFamilyIndices device_get_queue_families(Device *device);

void device_cleanup(Device *device);

#endif /* DEVICE_H */
