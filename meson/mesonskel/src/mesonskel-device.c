/*
 * mesonskel-device.c: A device
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:mesonskel-device
 * @title: 
 * @short_description:  type
 *
 * #mesonskel_device_t is a type representing...
 */

#include "mesonskel-device-private.h"

/**
 * mesonskel_device_create:
 *
 * Allocates a new #mesonskel_device_t.
 *
 * The contents of the returned device are undefined.
 *
 * @returns The newly allocated device, or @c NULL on error.
 *
 * Since: 0.0
 */
mesonskel_device_t *
mesonskel_device_create(void) {
  struct mesonskel_device_t *device;

  device = calloc(1, sizeof (*device));
  if (device == NULL)
    return NULL;

  return device;
}

/**
 * mesonskel_device_init:
 * @device: A #mesonskel_device_t to be initialized.
 *
 * Since: 0.0
 */
void
mesonskel_device_init(mesonskel_device_t *device) {
  /* TODO: Initialize the structure's values */
}

/**
 * mesonskel_device_destroy:
 * @device: A #mesonskel_device_t to be freed.
 *
 * Frees the resources allocated by mesonskel_device_create().
 *
 * Since: 0.0
 */
void
mesonskel_device_destroy(mesonskel_device_t *device) {
  free(device);
}

