/*
 * mesonskel-rectangle.h: A rectangle
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef MESONSKEL_RECTANGLE_H
#define MESONSKEL_RECTANGLE_H

mesonskel_rectangle_t *
mesonskel_rectangle_create();

void
mesonskel_rectangle_init(mesonskel_rectangle_t *rectangle);

void
mesonskel_rectangle_destroy(mesonskel_rectangle_t *rectangle);

#endif /* MESONSKEL_RECTANGLE_H */
