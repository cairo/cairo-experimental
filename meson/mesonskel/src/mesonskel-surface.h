/*
 * mesonskel-surface.h: A surface
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef MESONSKEL_SURFACE_H
#define MESONSKEL_SURFACE_H

mesonskel_surface_t *
mesonskel_surface_create();

void
mesonskel_surface_init(mesonskel_surface_t *surface);

void
mesonskel_surface_destroy(mesonskel_surface_t *surface);

#endif /* MESONSKEL_SURFACE_H */
