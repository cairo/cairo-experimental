/*
 * mesonskel-path.c: A path
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:mesonskel-path
 * @title: 
 * @short_description:  type
 *
 * #mesonskel_path_t is a type representing...
 */

#include "mesonskel-path-private.h"

/**
 * mesonskel_path_create:
 *
 * Allocates a new #mesonskel_path_t.
 *
 * The contents of the returned path are undefined.
 *
 * @returns The newly allocated path, or @c NULL on error.
 *
 * Since: 0.0
 */
mesonskel_path_t *
mesonskel_path_create(void) {
  struct mesonskel_path_t *path;

  path = calloc(1, sizeof (*path));
  if (path == NULL)
    return NULL;

  return path;
}

/**
 * mesonskel_path_init:
 * @path: A #mesonskel_path_t to be initialized.
 *
 * Since: 0.0
 */
void
mesonskel_path_init(mesonskel_path_t *path) {
  /* TODO: Initialize the structure's values */
}

/**
 * mesonskel_path_destroy:
 * @path: A #mesonskel_path_t to be freed.
 *
 * Frees the resources allocated by mesonskel_path_create().
 *
 * Since: 0.0
 */
void
mesonskel_path_destroy(mesonskel_path_t *path) {
  free(path);
}

