/*
 * mesonskel-path.h: A path
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef MESONSKEL_PATH_H
#define MESONSKEL_PATH_H

mesonskel_path_t *
mesonskel_path_create();

void
mesonskel_path_init(mesonskel_path_t *path);

void
mesonskel_path_destroy(mesonskel_path_t *path);

#endif /* MESONSKEL_PATH_H */
