/*
 * mesonskel-device-private.h: A device
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef MESONSKEL_DEVICE_H
#define MESONSKEL_DEVICE_H

/**
 * mesonskel_device_t:
 *
 * Since: 0.0
 */
typedef struct _mesonskel_device {
  // TODO
} mesonskel_device_t;

#endif /* MESONSKEL_DEVICE_H */
