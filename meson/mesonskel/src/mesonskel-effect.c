/*
 * mesonskel-effect.c: A effect
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:mesonskel-effect
 * @title: 
 * @short_description:  type
 *
 * #mesonskel_effect_t is a type representing...
 */

#include "mesonskel-effect-private.h"

/**
 * mesonskel_effect_create:
 *
 * Allocates a new #mesonskel_effect_t.
 *
 * The contents of the returned effect are undefined.
 *
 * @returns The newly allocated effect, or @c NULL on error.
 *
 * Since: 0.0
 */
mesonskel_effect_t *
mesonskel_effect_create(void) {
  struct mesonskel_effect_t *effect;

  effect = calloc(1, sizeof (*effect));
  if (effect == NULL)
    return NULL;

  return effect;
}

/**
 * mesonskel_effect_init:
 * @effect: A #mesonskel_effect_t to be initialized.
 *
 * Since: 0.0
 */
void
mesonskel_effect_init(mesonskel_effect_t *effect) {
  /* TODO: Initialize the structure's values */
}

/**
 * mesonskel_effect_destroy:
 * @effect: A #mesonskel_effect_t to be freed.
 *
 * Frees the resources allocated by mesonskel_effect_create().
 *
 * Since: 0.0
 */
void
mesonskel_effect_destroy(mesonskel_effect_t *effect) {
  free(effect);
}

