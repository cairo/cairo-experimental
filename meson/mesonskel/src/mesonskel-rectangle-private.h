/*
 * mesonskel-rectangle-private.h: A rectangle
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef MESONSKEL_RECTANGLE_H
#define MESONSKEL_RECTANGLE_H

/**
 * mesonskel_rectangle_t:
 *
 * Since: 0.0
 */
typedef struct _mesonskel_rectangle {
  // TODO
} mesonskel_rectangle_t;

#endif /* MESONSKEL_RECTANGLE_H */
