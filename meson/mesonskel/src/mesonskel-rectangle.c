/*
 * mesonskel-rectangle.c: A rectangle
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:mesonskel-rectangle
 * @title: 
 * @short_description:  type
 *
 * #mesonskel_rectangle_t is a type representing...
 */

#include "mesonskel-rectangle-private.h"

/**
 * mesonskel_rectangle_create:
 *
 * Allocates a new #mesonskel_rectangle_t.
 *
 * The contents of the returned rectangle are undefined.
 *
 * @returns The newly allocated rectangle, or @c NULL on error.
 *
 * Since: 0.0
 */
mesonskel_rectangle_t *
mesonskel_rectangle_create(void) {
  struct mesonskel_rectangle_t *rectangle;

  rectangle = calloc(1, sizeof (*rectangle));
  if (rectangle == NULL)
    return NULL;

  return rectangle;
}

/**
 * mesonskel_rectangle_init:
 * @rectangle: A #mesonskel_rectangle_t to be initialized.
 *
 * Since: 0.0
 */
void
mesonskel_rectangle_init(mesonskel_rectangle_t *rectangle) {
  /* TODO: Initialize the structure's values */
}

/**
 * mesonskel_rectangle_destroy:
 * @rectangle: A #mesonskel_rectangle_t to be freed.
 *
 * Frees the resources allocated by mesonskel_rectangle_create().
 *
 * Since: 0.0
 */
void
mesonskel_rectangle_destroy(mesonskel_rectangle_t *rectangle) {
  free(rectangle);
}

